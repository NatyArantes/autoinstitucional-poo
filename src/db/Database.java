package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.postgresql.Driver;

import model.Aluno;

public class Database {
	
	private String url;
	private String db;
	private String passwd;
	private String driver;
	private Connection conn;
	

	public static Connection objCon = null;
	private static PreparedStatement pstm;
	
	
	
	public static void abreConexao() throws SQLException {
//		DriverManager.registerDriver(new ());
		objCon = DriverManager.getConnection(
				"https://diariosis.c2eqcaqmrjbo.us-east-1.rds.amazonaws.com",
				"pgadmin",
				"pgadministrator");
	}
	
	public static void fechaConexao() throws SQLException {
		objCon.close();
	}
	
	public void adicionaAluno(Aluno aluno) throws SQLException {
		String queryCPF = "SELECT * FROM Aluno WHERE Matricula = ?";
		pstm = objCon.prepareStatement(queryCPF, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		pstm.setString(1, aluno.getMatricula());
		ResultSet resultCPF = pstm.executeQuery();
		
		if(resultCPF.next()) {
			throw new SQLException("Aluno já cadastrado.");
		}
		
		String atualizaSQL = "INSERT INTO Aluno (Matricula, Nome, Endereco) VALUES (?, ?, ?)";
		pstm = objCon.prepareStatement(atualizaSQL);
		pstm.setString (1, aluno.getMatricula());
		pstm.setString(2, aluno.getNome());
		pstm.setString(3, aluno.getEndereco());
		
	}
}

